﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Imager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string[] AsciiChars = { "#", "#", "@", "%", "=", "+", "*", ":", "-", ".", " " };
        private string Content;
        
        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG;*.JPEG)|*.BMP;*.JPG;*.GIF;*.PNG;*.JPEG";
            
            DialogResult diag = openFileDialog1.ShowDialog();
            
            if (diag == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
                button2.Enabled = true;
            }         
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            button2.Enabled = false;
            Bitmap image = new Bitmap(textBox1.Text, true);
            try
            {
                int valueText = int.Parse(textBox2.Text);
                if (valueText < 0 | valueText > 1500) throw new Exception();
                image = GetReSizedImage(image, valueText);
                Content = ConvertToAscii(image);
                webBrowser1.DocumentText = "<pre>" + Content + "</pre>";

            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch
            {
                MessageBox.Show("Enter number from 1 to 1500.");
            }

            button2.Enabled = true;
        }
                      
        private void button3_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Text file (*.txt)|*.txt";
           
            DialogResult dr = saveFileDialog1.ShowDialog();
            
            if (dr == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(saveFileDialog1.FileName);
                sw.Write(Content);
                sw.Flush();
                sw.Close();
            }
        }
        private Bitmap GetReSizedImage(Bitmap inputBitmap, int asciiWidth)
        {
            int asciiHeight = 0;

            asciiHeight = (int)Math.Ceiling((double)inputBitmap.Height * asciiWidth / inputBitmap.Width);

            Bitmap result = new Bitmap(asciiWidth, asciiHeight);
            Graphics g = Graphics.FromImage((Image)result);
                        
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.DrawImage(inputBitmap, 0, 0, asciiWidth, asciiHeight);
            g.Dispose();
            return result;
        }
        private string ConvertToAscii(Bitmap image)
        {
            Boolean toggle = false;
            StringBuilder sb = new StringBuilder();

            for (int h = 0; h < image.Height; h++)
            {
                for (int w = 0; w < image.Width; w++)
                {
                    Color pixelColor = image.GetPixel(w, h);
                    
                    int alpha = pixelColor.A;
                    int red = pixelColor.R;
                    int green = pixelColor.G;
                    int blue = pixelColor.B;
                    int avg = (red + green + blue) / 3;
                    
                    Color grayColor = Color.FromArgb(alpha, avg, avg, avg);
                    
                    if (!toggle)
                    {
                        int index = (int)Math.Ceiling(((double)avg * 10) / 255);
                        sb.Append(AsciiChars[index]);
                    }
                }
                if (!toggle)
                {
                    sb.Append("\r\n");
                    toggle = true;
                }
                else
                {
                    toggle = false;
                }
            }

            image.Dispose();
            return sb.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
